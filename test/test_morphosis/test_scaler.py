import pandas as pd
import numpy as np
import pytest

from morphosis.scaler import HardMinMaxScaler, MultiHotEncoder


class TestHardMinMaxScaler:
    def test_hard_min_max(self):
        data = pd.DataFrame({"feature_1": [-2, -2, -3], "feature_2": [5, 2, 1]})

        scaler = HardMinMaxScaler()
        x = scaler.fit(data)

        data = data.replace({"feature_1": {x.data_min_[0]: x.data_min_[0] - 1}})
        data = data.replace({"feature_2": {x.data_max_[1]: x.data_max_[1] + 1}})

        response = scaler.transform(data)

        assert np.max(response) <= x.capping[1]
        assert np.min(response) >= x.capping[0]


class TestMultiHotEncoder:
    def test_multi_hot_encoder(self):
        data = pd.DataFrame({"feature_1": [[1, 2], [2, 3], [2]]})

        scaler = MultiHotEncoder()
        x = scaler.fit(data)
        assert np.array_equal(x.classes_[0], np.array([1, 2, 3]))

        trans = scaler.transform(data)
        assert np.array_equal(trans, np.array([[1, 1, 0], [0, 1, 1], [0, 1, 0]]))

    def test_multi_hot_encoder_literal_eval(self):
        data = pd.DataFrame({"feature_1": [[1, 2], [2, 3], [2]]})
        data_str = pd.DataFrame({"feature_2": ["[1, 2]", "[2, 3]", "[2]"]})

        scaler = MultiHotEncoder()
        x = scaler.fit(data)

        scaler_str = MultiHotEncoder()
        x_str = scaler_str.fit(data_str)
        assert np.array_equal(x.classes_[0], x_str.classes_[0])

    def test_multi_hot_encoder_non_iterable(self):
        data = pd.DataFrame({"feature_1": ["1, 2", "2, 3", "2"]})
        scaler = MultiHotEncoder()
        with pytest.raises(TypeError):
            scaler.fit(data)
